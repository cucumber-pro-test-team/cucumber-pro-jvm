package cucumber.pro;

import cucumber.pro.results.BuildIdGenerator;
import cucumber.pro.results.MetadataApi;
import cucumber.pro.results.NullMetadataClient;
import cucumber.pro.results.Status;
import cucumber.pro.results.WebSocketMetadataClient;
import cucumber.pro.scm.WorkingCopy;
import cucumber.pro.scm.WorkingCopyDetector;
import cucumber.runtime.CucumberException;
import cucumber.runtime.Env;
import gherkin.formatter.Formatter;
import gherkin.formatter.Reporter;
import gherkin.formatter.model.Background;
import gherkin.formatter.model.BasicStatement;
import gherkin.formatter.model.Examples;
import gherkin.formatter.model.Feature;
import gherkin.formatter.model.Match;
import gherkin.formatter.model.Result;
import gherkin.formatter.model.Scenario;
import gherkin.formatter.model.ScenarioOutline;
import gherkin.formatter.model.Step;

import javax.websocket.DeploymentException;
import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static cucumber.pro.results.Debug.debug;
import static java.util.Arrays.asList;

public class CucumberProFormatter implements Formatter, Reporter {
    private static final List<String> BUILD_NUMBER_ENV_VARS = asList("BUILD_NUMBER", "CIRCLE_BUILD_NUM", "TRAVIS_JOB_NUMBER", "bamboo.buildNumber", "CI_BUILD_NUMBER");
    private static Env ENV = new Env("cucumber");
    private static final int REPLY_TIMEOUT_MILLIS = 10000;

    private static URI resultsUri() throws URISyntaxException {
        return new URI(ENV.get("CUCUMBER_PRO_RESULTS_URL", "wss://results.cucumber.pro/ws"));
    }

    private static String getToken() {
        return ENV.get("CUCUMBER_PRO_TOKEN");
    }

    private static boolean shouldPublish() {
        return getToken() != null && isContinuousIntegration();
    }

    private static boolean isContinuousIntegration() {
        return (getBuildNumberFromEnvironment() != null || ENV.get("CI") != null);
    }

    private final MetadataApi metadataApi;
    private final WorkingCopy workingCopy;
    private final BuildIdGenerator buildIdGenerator;

    // These should be cleared out
    private List<Step> steps = new ArrayList<>();
    private BasicStatement statement;
    private String uri;
    private boolean headerSent;
    private List<Status> statuses = new ArrayList<>();
    private int currentLine;

    private static MetadataApi createMetadataApi() throws URISyntaxException, IOException, DeploymentException {
        if (shouldPublish()) {
            return new WebSocketMetadataClient(new URI(resultsUri().toString() + "?token=" + getToken()));
        } else {
            return new NullMetadataClient();
        }
    }

    public CucumberProFormatter() throws Exception {
        this(createMetadataApi(), WorkingCopyDetector.detect(), BuildIdGenerator.DEFAULT);
    }

    CucumberProFormatter(MetadataApi metadataApi, WorkingCopy workingCopy, BuildIdGenerator buildIdGenerator) {
        debug("Cucumber Pro formatter is enabled");
        this.metadataApi = metadataApi;
        this.workingCopy = workingCopy;
        this.buildIdGenerator = buildIdGenerator;
        if (shouldPublish() && !"NO".equalsIgnoreCase(ENV.get("CHECK_CLEAN"))) {
            debug("Checking if the working copy is clean");
            workingCopy.checkClean();
            debug("Clean! Publishing results");
        } else {
            debug("Not publishing results. Token is missing or CI environment not detected");
        }
    }

    // Reporter API

    @Override
    public void before(Match match, Result result) {
        // Need to move format() up in CucumberScenario so statement isn't null.
//        metadataApi.publishLineResult(uri, statement.getLine(), result.getStatus(), attachments);
    }

    @Override
    public void result(final Result result) {
        publishResult(result, steps.remove(0));
    }

    @Override
    public void after(Match match, Result result) {
        publishResult(result, statement);
    }

    private void publishResult(Result result, BasicStatement stmt) {
        Status status = getStatus(result);
        statuses.add(status);

        metadataApi.sendTestStepResult(uri, stmt.getLine() + 1, status);

        String errorMessage = result.getErrorMessage();
        if (errorMessage != null) {
            metadataApi.sendStackTrace(uri, stmt.getLine() + 1, errorMessage);
        }
    }

    @Override
    public void match(Match match) {
    }

    @Override
    public void embedding(String mimeType, byte[] bytes) {
        metadataApi.sendMetadata(uri, currentLine + 1, mimeType);

        metadataApi.sendBinary(bytes);
    }

    @Override
    public void write(String text) {
    }

    // Formatter

    @Override
    public void uri(String uri) {
        this.uri = uri;
    }

    @Override
    public void feature(Feature feature) {
        if (!headerSent) {
            String buildNumber = getBuildNumberFromEnvironment();
            if (buildNumber == null) {
                buildNumber = buildIdGenerator.buildId();
            }
            metadataApi.sendHeader(workingCopy.getRepoUrl(), workingCopy.getRev(), workingCopy.getBranch(), buildNumber);
            headerSent = true;
        }
    }

    private static String getBuildNumberFromEnvironment() {
        for (String var : BUILD_NUMBER_ENV_VARS) {
            String buildId = ENV.get(var);
            if (buildId != null) {
                return buildId;
            }
        }
        return null;
    }

    @Override
    public void background(Background background) {
    }

    @Override
    public void scenario(Scenario scenario) {
        this.statement = scenario;
    }

    @Override
    public void scenarioOutline(ScenarioOutline scenarioOutline) {
    }

    @Override
    public void examples(Examples examples) {
    }

    @Override
    public void startOfScenarioLifeCycle(Scenario scenario) {
        statuses.clear();
    }

    @Override
    public void step(Step step) {
        currentLine = step.getLine();
        steps.add(step);
    }

    @Override
    public void endOfScenarioLifeCycle(Scenario scenario) {
        metadataApi.sendTestCaseResult(uri, scenario.getLine() + 1, getStatus());
    }

    private Status getStatus() {
        int pos = 0;
        for (Status status : statuses) {
            int p = Arrays.binarySearch(Status.values(), status);
            pos = Math.max(pos, p);
        }
        return Status.values()[pos];
    }

    private Status getStatus(Result result) {
        return Status.valueOf(result.getStatus());
    }


    @Override
    public void eof() {
    }

    @Override
    public void syntaxError(String state, String event, List<String> legalEvents, String uri, Integer line) {
    }

    @Override
    public void done() {
        try {
            if(shouldPublish()) {
                debug("Results successfully published to Cucumber Pro");
            } else {
                debug("No results published to Cucumber Pro");
            }
            metadataApi.waitForReplies(REPLY_TIMEOUT_MILLIS);
        } catch (InterruptedException e) {
            throw new CucumberException(e);
        }
    }

    @Override
    public void close() {
    }
}
