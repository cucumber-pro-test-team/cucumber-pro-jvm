package cucumber.pro;

import java.util.ResourceBundle;

public class Version {
    public static final String VERSION = ResourceBundle.getBundle("cucumber.pro.version").getString("cucumber-pro-jvm.version");
}
