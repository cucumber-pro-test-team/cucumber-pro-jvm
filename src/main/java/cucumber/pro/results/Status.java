package cucumber.pro.results;

public enum Status {
    // Statuses in order of severity, least severe first.
    passed,
    skipped,
    pending,
    undefined,
    failed,
}
