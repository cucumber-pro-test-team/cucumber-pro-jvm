package cucumber.pro.results;

public class Header {
    private final String repo_url;
    private final String rev;
    private final String branch;
    private final String build_id;
    private final Info info = new Info();

    public Header(String repoUrl, String rev, String branch, String build_id) {
        this.repo_url = repoUrl;
        this.rev = rev;
        this.branch = branch;
        this.build_id = build_id;
    }
}
