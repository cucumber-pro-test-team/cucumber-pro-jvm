package cucumber.pro.results.metadata;

import cucumber.pro.results.Status;

public class TestStepResult extends Result {
    public static final String MIME_TYPE = "application/vnd.cucumber.test-step-result+json";

    public TestStepResult(String path, int location, Status status) {
        super(path, location, MIME_TYPE, status);
    }
}
