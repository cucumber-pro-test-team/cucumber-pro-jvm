package cucumber.pro.results.metadata;

public class StackTrace extends JsonBody {
    public static final String MIME_TYPE = "text/vnd.cucumber-pro.stacktrace.jvm+plain";

    public StackTrace(String path, int location, String stackTrace) {
        super(path, location, MIME_TYPE);
        body.put("text", stackTrace);
    }
}
