package cucumber.pro.results.metadata;

import cucumber.pro.results.Status;

public class TestCaseResult extends Result {
    public static final String MIME_TYPE = "application/vnd.cucumber-pro.test-case-result+json";

    public TestCaseResult(String path, int location, Status status) {
        super(path, location, MIME_TYPE, status);
    }
}
