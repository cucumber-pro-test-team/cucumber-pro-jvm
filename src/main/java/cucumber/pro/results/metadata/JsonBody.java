package cucumber.pro.results.metadata;

import java.util.HashMap;
import java.util.Map;

public class JsonBody extends Metadata {
    protected final Map<String, Object> body = new HashMap<>();

    public JsonBody(String path, int location, String mimeType) {
        super(path, location, mimeType);
    }
}
