package cucumber.pro.results.metadata;

public class Metadata {
    private final String path;
    private final int location;
    private final String mime_type;

    public Metadata(String path, int location, String mimeType) {
        this.path = path;
        this.location = location;
        this.mime_type = mimeType;
    }
}
