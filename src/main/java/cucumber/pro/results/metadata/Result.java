package cucumber.pro.results.metadata;

import cucumber.pro.results.Status;

public abstract class Result extends JsonBody {
    public Result(String path, int location, String mimeType, Status status) {
        super(path, location, mimeType);
        body.put("status", status);
    }
}
