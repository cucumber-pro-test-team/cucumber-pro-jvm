package cucumber.pro.results;

import gherkin.deps.com.google.gson.Gson;

import javax.websocket.DecodeException;
import javax.websocket.Decoder;
import javax.websocket.EndpointConfig;
import java.util.Map;

public class JsonDecoder implements Decoder.Text<Response> {
    private static final Gson GSON = new Gson();

    @Override
    public Response decode(String message) throws DecodeException {
        Map map = GSON.fromJson(message, Map.class);
        return new Response(map);
    }

    @Override
    public boolean willDecode(String message) {
        return true;
    }

    @Override
    public void init(EndpointConfig config) {
    }

    @Override
    public void destroy() {
    }
}
