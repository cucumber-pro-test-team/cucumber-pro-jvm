package cucumber.pro.results;

public class NullMetadataClient implements MetadataApi {
    @Override
    public void sendHeader(String repoUrl, String rev, String branch, String buildId) {
    }

    @Override
    public void sendTestStepResult(String path, int location, Status status) {
    }

    @Override
    public void sendTestCaseResult(String path, int location, Status status) {
    }

    @Override
    public void sendStackTrace(String path, int location, String stackTrace) {
    }

    @Override
    public void sendMetadata(String path, int location, String mimeType) {
    }

    @Override
    public void sendBinary(byte[] attachment) {
    }

    @Override
    public void waitForReplies(int timeoutMillis) {
    }
}
