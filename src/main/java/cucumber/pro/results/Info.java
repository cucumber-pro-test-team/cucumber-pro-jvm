package cucumber.pro.results;

import cucumber.pro.Version;
import cucumber.runtime.RuntimeOptions;

public class Info {
    private final String os = System.getProperty("os.name") + " " + System.getProperty("os.version") + " (" + System.getProperty("os.arch") + ")";
    private final String platform_version = "Java " + System.getProperty("java.version") + " (" + System.getProperty("java.vendor") + ")";
    private final String tool_version = "cucumber-jvm " + RuntimeOptions.VERSION;
    private final String client_version = "cucumber-pro-jvm " + Version.VERSION;
    private final String os_user = System.getProperty("user.name");
}
