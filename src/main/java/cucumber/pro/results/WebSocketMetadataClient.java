package cucumber.pro.results;

import cucumber.pro.results.metadata.Metadata;
import cucumber.pro.results.metadata.StackTrace;
import cucumber.pro.results.metadata.TestCaseResult;
import cucumber.pro.results.metadata.TestStepResult;
import cucumber.runtime.CucumberException;
import cucumber.runtime.Env;
import org.glassfish.tyrus.client.ClientManager;
import org.glassfish.tyrus.core.HandshakeException;

import javax.websocket.ClientEndpoint;
import javax.websocket.CloseReason;
import javax.websocket.DeploymentException;
import javax.websocket.OnClose;
import javax.websocket.OnError;
import javax.websocket.OnMessage;
import javax.websocket.RemoteEndpoint;
import javax.websocket.Session;
import java.io.IOException;
import java.net.InetAddress;
import java.net.URI;
import java.net.URISyntaxException;
import java.nio.ByteBuffer;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Future;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;
import java.util.concurrent.locks.Condition;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

import static cucumber.pro.results.Debug.debug;

@ClientEndpoint(encoders = JsonEncoder.class, decoders = JsonDecoder.class)
public class WebSocketMetadataClient implements MetadataApi {
    private static Env ENV = new Env("cucumber");
    private static final long CONNECTION_TIMEOUT_MILLIS = Long.parseLong(ENV.get("CUCUMBER_PRO_CONNECTION_TIMEOUT_MILLIS", "10000"));
    private final Future<Session> sessionFuture;
    private final URI uri;

    private final Lock lock = new ReentrantLock();
    private final Condition noPendingMessages = lock.newCondition();
    private int pendingMessageCount = 0;
    private Throwable protocolError;

    public WebSocketMetadataClient(URI uri) throws URISyntaxException, IOException, DeploymentException {
        this.uri = uri;
        // Resolve the host address before trying to connect, giving DNS a kick.
        // This is to avoid connection timeouts when the host name is not in the DNS cache.
        debug("Resolving hostname %s", uri.getHost());
        String hostAddress = InetAddress.getByName(uri.getHost()).getHostAddress();
        debug("Resolved to %s", hostAddress);

        ClientManager client = ClientManager.createClient();
        sessionFuture = client.asyncConnectToServer(this, uri);
    }

    @Override
    public void sendHeader(String repoUrl, String rev, String branch, String buildId) {
        sendObject(new Header(repoUrl, rev, branch, buildId));
    }

    @Override
    public void sendTestStepResult(String path, int location, Status status) {
        sendObject(new TestStepResult(path, location, status));
    }

    @Override
    public void sendTestCaseResult(String path, int location, Status status) {
        sendObject(new TestCaseResult(path, location, status));
    }

    @Override
    public void sendStackTrace(String path, int location, String stackTrace) {
        sendObject(new StackTrace(path, location, stackTrace));
    }

    @Override
    public void sendMetadata(String path, int location, String mimeType) {
        sendObject(new Metadata(path, location, mimeType));
    }

    @Override
    public void sendBinary(byte[] attachment) {
        getEndpoint().sendBinary(ByteBuffer.wrap(attachment));
        incPendingMessages();
    }

    private void sendObject(Object data) {
        getEndpoint().sendObject(data);
        incPendingMessages();
    }

    private void incPendingMessages() {
        try {
            lock.lock();
            ++pendingMessageCount;
        } finally {
            lock.unlock();
        }
    }

    @OnMessage
    public void onMessage(Response response) {
        CucumberException exception = response.getException();
        if (exception != null) {
            setError(exception);
        } else {
            try {
                lock.lock();
                --pendingMessageCount;
                if (pendingMessageCount == 0) {
                    noPendingMessages.signal();
                }
            } finally {
                lock.unlock();
            }
        }
    }

    @Override
    public void waitForReplies(int timeoutMillis) throws InterruptedException {
        try {
            lock.lock();
            debug("Waiting for in-flight acks: %s", pendingMessageCount);
            noPendingMessages.await(timeoutMillis, TimeUnit.MILLISECONDS);
            if (pendingMessageCount > 0) {
                throw new CucumberException("Timed out waiting for replies from Cucumber Pro. Pending messages: " + pendingMessageCount);
            }
            if (protocolError != null) {
                throw new CucumberException(protocolError);
            }
            debug("All acks received");
        } finally {
            lock.unlock();
        }
    }

    private RemoteEndpoint.Async getEndpoint() {
        try {
            Session session = sessionFuture.get(CONNECTION_TIMEOUT_MILLIS, TimeUnit.MILLISECONDS);
            if (session == null) throw new CucumberException("Couldn't connect to " + uri);
            return session.getAsyncRemote();
        } catch (ExecutionException e) {
            if (e.getCause() instanceof DeploymentException) {
                DeploymentException de = (DeploymentException) e.getCause();
                if (de.getCause() instanceof HandshakeException) {
                    HandshakeException he = (HandshakeException) de.getCause();

                    switch (he.getHttpStatusCode()) {
                        case 401:
                            throw new CucumberException("Couldn't authenticate user: " + uri.getQuery());
                        case 403:
                            throw new CucumberException("Access denied: " + uri.getQuery());
                        default:
                            throw he;
                    }
                } else {
                    throw new CucumberException(e);
                }
            } else {
                throw new CucumberException(e);
            }
        } catch (TimeoutException e) {
            throw new CucumberException("Timed out connecting to " + uri, e);
        } catch (Exception e) {
            throw new CucumberException(e);
        }
    }

    @OnError
    public void onError(Throwable t) {
        setError(t);
    }

    private void setError(Throwable t) {
        if (protocolError == null) {
            try {
                lock.lock();
                protocolError = t;
                pendingMessageCount = 0;
                noPendingMessages.signal();
            } finally {
                lock.unlock();
            }
        }
    }


    @OnClose
    public void onClose(CloseReason reason) {
        if (reason.getCloseCode().getCode() == 1002) {
            // Protocol error
            setError(new CucumberException("Protocol error"));
        }
        sessionFuture.cancel(true);
    }
}
