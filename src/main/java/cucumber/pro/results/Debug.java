package cucumber.pro.results;

import cucumber.runtime.Env;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.TimeZone;

public class Debug {
    private static final Env ENV = new Env("cucumber");
    private static final boolean on = Boolean.parseBoolean(ENV.get("CUCUMBER_PRO_DEBUG"));
    private static final TimeZone tz = TimeZone.getTimeZone("UTC");
    private static final DateFormat df = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm'Z'");
    static {
        df.setTimeZone(tz);
    }

    public static void debug(String text, Object... args) {
        if (on) {
            System.out.format("[" + df.format(new Date()) + "] [Cucumber Pro Plugin] " + text + "\n", args);
        }
    }
}
