package cucumber.pro.results;

public interface MetadataApi {
    void sendHeader(String repoUrl, String rev, String branch, String buildId);

    void sendTestStepResult(String path, int location, Status status);

    void sendTestCaseResult(String path, int location, Status status);

    void sendStackTrace(String path, int location, String stackTrace);

    void sendMetadata(String path, int location, String mimeType);

    void sendBinary(byte[] attachment);

    void waitForReplies(int timeoutMillis) throws InterruptedException;
}
