package cucumber.pro.results;

import cucumber.runtime.CucumberException;

import java.util.Map;

public class Response {

    private final Map<String, String> map;

    public Response(Map<String, String> map) {
        this.map = map;
    }

    public Map<String, String> getMap() {
        return map;
    }

    public CucumberException getException() {
        return map.containsKey("error") ? new CucumberException(map.get("error")) : null;
    }
}
