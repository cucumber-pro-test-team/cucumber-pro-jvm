package cucumber.pro.results;

import gherkin.deps.com.google.gson.Gson;

import javax.websocket.EncodeException;
import javax.websocket.Encoder;
import javax.websocket.EndpointConfig;

public class JsonEncoder<T> implements Encoder.Text<T> {
    private static final Gson GSON = new Gson();

    @Override
    public String encode(T message) throws EncodeException {
        return GSON.toJson(message);
    }

    @Override
    public void init(EndpointConfig config) {
    }

    @Override
    public void destroy() {
    }
}
