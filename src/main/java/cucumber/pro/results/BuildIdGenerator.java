package cucumber.pro.results;

import java.util.UUID;

public interface BuildIdGenerator {
    String buildId();

    public static BuildIdGenerator DEFAULT = new BuildIdGenerator() {
        @Override
        public String buildId() {
            return UUID.randomUUID().toString();
        }
    };
}
