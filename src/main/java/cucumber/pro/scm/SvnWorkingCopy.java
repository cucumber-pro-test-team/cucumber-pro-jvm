package cucumber.pro.scm;

import java.io.File;
import java.util.List;

class SvnWorkingCopy implements WorkingCopy {

    private final File rootDir;
    private final Exec exec;

    public static SvnWorkingCopy detect(File dir) {
        File workingCopyRoot = null;
        while (dir != null) {
            File dotSvn = new File(dir, ".svn");
            if (dotSvn.isDirectory()) {
                workingCopyRoot = dir;
            }
            dir = dir.getParentFile();
        }
        return workingCopyRoot != null ? new SvnWorkingCopy(workingCopyRoot) : null;
    }

    public SvnWorkingCopy(File rootDir) {
        this.rootDir = rootDir;
        this.exec = new Exec(rootDir);
    }

    @Override
    public String getRev() {
        List<String> stdout = exec.cmd("svn info");
        for (String line : stdout) {
            if (line.startsWith("Revision: ")) {
                return line.substring("Revision: ".length());
            }
        }
        throw new RuntimeException("Couldn't detect revision");
    }

    @Override
    public String getRepoUrl() {
        List<String> stdout = exec.cmd("svn info");
        for (String line : stdout) {
            if (line.startsWith("URL: ")) {
                return line.substring("URL: ".length());
            }
        }
        throw new RuntimeException("Couldn't detect revision");
    }

    @Override
    public String getBranch() {
        String url = null, repositoryRoot = null;

        List<String> stdout = exec.cmd("svn info");
        for (String line : stdout) {
            if (line.startsWith("URL: ")) {
                url = line.substring("URL: ".length());
            }
            if (line.startsWith("Repository Root: ")) {
                repositoryRoot = line.substring("Repository Root: ".length());
            }
        }
        if (url == null) throw new RuntimeException("No URL found");
        if (repositoryRoot == null) throw new RuntimeException("No Repository Root found");
        return url.substring(repositoryRoot.length() + 1);
    }

    @Override
    public void checkClean() {
        List<String> status = exec.cmd("svn status");
        for (String line : status) {
            if (!line.startsWith("?")) {
                // We'll allow unknown files since builds create them, but
                // modified/added/deleted files are not allowed.
                throw new RuntimeException("Please commit your changes before running with the Cucumber Pro formatter.");
            }
        }
    }

    File getRootDir() {
        return rootDir;
    }
}
