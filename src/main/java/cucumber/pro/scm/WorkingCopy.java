package cucumber.pro.scm;

public interface WorkingCopy {
    String getRev();

    String getRepoUrl();

    String getBranch();

    void checkClean();
}
