package cucumber.pro.scm;

import java.io.File;
import java.util.List;

class GitWorkingCopy implements WorkingCopy {
    private final File rootDir;
    private final Exec exec;

    public static GitWorkingCopy detect(File dir) {
        File workingCopyRoot = null;
        while (dir != null) {
            File dotSvn = new File(dir, ".git");
            if (dotSvn.isDirectory()) {
                workingCopyRoot = dir;
            }
            dir = dir.getParentFile();
        }
        return workingCopyRoot != null ? new GitWorkingCopy(workingCopyRoot) : null;
    }

    public GitWorkingCopy(File rootDir) {
        this.rootDir = rootDir;
        this.exec = new Exec(rootDir);
    }

    @Override
    public String getRev() {
        return exec.cmd("git rev-parse HEAD").get(0);
    }

    @Override
    public String getRepoUrl() {
        // First, loop through all remotes
        List<String> remotes = exec.cmd("git ls-remote --get-url");
        for (String remote : remotes) {
            if (remote.contains("github") || remote.contains("bitbucket")) {
                // That's a good one.
                return remote;
            }
        }
        // Fall back to the origin url
        return exec.cmd("git config --get remote.origin.url").get(0);
    }

    @Override
    public String getBranch() {
        String rawBranch = getRawBranch();
        String[] branchElements = rawBranch.split("/");
        return branchElements[branchElements.length - 1];
    }

    private String getRawBranch() {
        // CI servers typically check out a commit, so `git branch` won't work.
        // Output is "* branchname" so we strip the 2 first chars
        List<String> branches = exec.cmd("git branch --contains " + getRev());
        for (String branch : branches) {
            if (!branch.contains("* (detached from")) {
                return branch.substring(2);
            }
        }
        // Fall back to this:
        return exec.cmd("git name-rev --name-only HEAD").get(0);
    }

    @Override
    public void checkClean() {
        checkNoModifications();
        checkCurrentBranchPushed();
    }

    private void checkNoModifications() {
        List<String> dirtyFiles = exec.cmd("git status --untracked-files=no --porcelain");
        if (!dirtyFiles.isEmpty()) {
            throw new RuntimeException("Please commit and push your changes before running with the Cucumber Pro formatter.");
        }
    }

    // http://stackoverflow.com/questions/2016901/viewing-unpushed-git-commits
    private void checkCurrentBranchPushed() {
        String branch = getBranch();
        List<String> unpushedCommits = exec.cmd(String.format("git log origin/%s..%s --oneline", branch, branch));
        if (!unpushedCommits.isEmpty()) {
            throw new RuntimeException("Your current branch has commits that haven't been pushed to origin. Please push your changes before running with the Cucumber Pro formatter.");
        }
    }


    File getRootDir() {
        return rootDir;
    }
}
