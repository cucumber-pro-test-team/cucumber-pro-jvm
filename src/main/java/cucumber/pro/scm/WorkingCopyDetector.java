package cucumber.pro.scm;

import java.io.File;

public class WorkingCopyDetector {
    private static final File PWD = new File(System.getProperty("user.dir"));

    public static WorkingCopy detect() {
        WorkingCopy result = GitWorkingCopy.detect(PWD);
        if (result == null) {
            result = SvnWorkingCopy.detect(PWD);
        }
        if (result == null) {
            throw new RuntimeException("No Git or Subversion working copy detected in " + PWD.getAbsolutePath());
        }
        return result;
    }
}
