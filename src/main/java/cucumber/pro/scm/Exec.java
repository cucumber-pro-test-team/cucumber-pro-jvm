package cucumber.pro.scm;

import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;

class Exec {
    public final File dir;

    public Exec(File dir) {
        this.dir = dir;
    }

    public List<String> cmd(String cmd) {
        try {
            List<String> stdoutLines = new ArrayList<>();
            Process process = Runtime.getRuntime().exec(cmd, new String[0], dir);
            BufferedReader stdout = new BufferedReader(new InputStreamReader(process.getInputStream(), "utf-8"));
            String line;
            while ((line = stdout.readLine()) != null) {
                stdoutLines.add(line);
            }
            return stdoutLines;
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }
}
