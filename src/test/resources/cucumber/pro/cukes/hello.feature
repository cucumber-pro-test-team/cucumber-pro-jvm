# Hello
Feature: Hello

  This is just a dummy feature that we'll
  run as part of Continuous Integration to provide
  an end-to-end test and see results being pushed
  through to Cucumber Pro.

  Scenario: Push through an undefined one
    Given this step passes
    When I check Cucumber Pro for results
    Then I should see the results for the first step

  Scenario: Push through a failing one
    Given this step passes
