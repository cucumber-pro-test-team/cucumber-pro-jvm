package cucumber.pro.results;

import org.junit.Test;

import java.util.HashMap;
import java.util.Map;

import static org.junit.Assert.assertNotNull;

public class ResponseTest {
    @Test
    public void creates_exception_if_reply_has_error_property() {
        Map<String, String> map = new HashMap<>();
        map.put("error", "This is the error");
        Response response = new Response(map);
        assertNotNull(response.getException());
    }
}
