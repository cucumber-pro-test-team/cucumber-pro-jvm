package cucumber.pro.results;

import gherkin.deps.com.google.gson.Gson;
import org.junit.After;
import org.junit.Test;
import org.webbitserver.BaseWebSocketHandler;
import org.webbitserver.WebServer;
import org.webbitserver.WebSocketConnection;
import org.webbitserver.netty.NettyWebServer;

import java.net.URI;
import java.util.Map;
import java.util.concurrent.ExecutionException;

public class WebsocketResultsClientTest {

    private WebServer webServer;

    @Test
    public void waits_for_reply_from_each_message() throws Exception {
        webServer = new NettyWebServer(3013);
        webServer.add("/ws", new BaseWebSocketHandler() {
            @Override
            public void onMessage(WebSocketConnection connection, String msg) throws Throwable {
                Map map = new Gson().fromJson(msg, Map.class);
                boolean isHeader = map.containsKey("repo_url");
                if (isHeader) {
                    connection.send("{\"type\": \"session_ready\"}");
                } else {
                    if (map.containsKey("body")) {
                        connection.send("{\"type\": \"metadata_saved\"}");
                    } else {
                        connection.send("{\"type\": \"metadata_awaiting_body\"}");
                    }
                }
            }

            @Override
            public void onMessage(WebSocketConnection connection, byte[] msg) throws Throwable {
                connection.send("{\"type\": \"metadata_saved\"}");
            }
        });
        webServer.start().get();

        URI uri = new URI("ws://localhost:3013/ws?token=topsecret");
        WebSocketMetadataClient client = new WebSocketMetadataClient(uri);

        client.sendHeader("memory://metarepo/test", "1", "master", "run-1");
        client.sendTestCaseResult("hello/world.feature", 2, Status.passed);
        client.sendMetadata("hello/world.feature", 2, "image/png");
        client.sendBinary("Hello".getBytes("UTF-8"));

        client.waitForReplies(1000);
    }

    @After
    public void stopServer() throws ExecutionException, InterruptedException {
        webServer.stop().get();
    }
}
