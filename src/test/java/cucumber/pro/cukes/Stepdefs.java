package cucumber.pro.cukes;

import cucumber.api.Scenario;
import cucumber.api.java.Before;
import cucumber.api.java.en.Given;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;

import static org.junit.Assert.fail;

public class Stepdefs {
    private Scenario scenario;

    @Before
    public void storeScenario(Scenario scenario) {
        this.scenario = scenario;
    }

    @Given("^this step passes$")
    public void this_step_passes() throws Throwable {
        InputStream alfredStream = getClass().getResourceAsStream("alfred-e-neuman-for-president.jpg");
        scenario.embed(read(alfredStream), "image/jpeg");
    }

    @Given("^this step fails$")
    public void this_step_fails() throws Throwable {
        fail("Oh noes");
    }

    private byte[] read(InputStream is) throws IOException {
        ByteArrayOutputStream buffer = new ByteArrayOutputStream();
        int read;
        byte[] data = new byte[16384];
        while ((read = is.read(data, 0, data.length)) != -1) {
            buffer.write(data, 0, read);
        }
        buffer.flush();
        return buffer.toByteArray();
    }
}
