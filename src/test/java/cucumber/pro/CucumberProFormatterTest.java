package cucumber.pro;

import cucumber.pro.results.BuildIdGenerator;
import cucumber.pro.results.MetadataApi;
import cucumber.pro.results.Status;
import cucumber.pro.scm.WorkingCopy;
import gherkin.formatter.model.Comment;
import gherkin.formatter.model.Feature;
import gherkin.formatter.model.Result;
import gherkin.formatter.model.Scenario;
import gherkin.formatter.model.Step;
import gherkin.formatter.model.Tag;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

import static org.mockito.Matchers.anyString;
import static org.mockito.Matchers.eq;
import static org.mockito.Matchers.startsWith;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

public class CucumberProFormatterTest {
    private static final List<Comment> C = new ArrayList<>();
    private static final List<Tag> T = new ArrayList<>();
    private static final String K = null;
    private static final String N = null;
    private static final String D = null;
    private static final String I = null;

    private MetadataApi metadataApi = mock(MetadataApi.class);
    private WorkingCopy workingCopy = mock(WorkingCopy.class);
    private BuildIdGenerator buildIdGenerator = mock(BuildIdGenerator.class);
    private CucumberProFormatter f = new CucumberProFormatter(metadataApi, workingCopy, buildIdGenerator);

    @Test
    public void sends_header_on_first_feature() {
        when(workingCopy.getRepoUrl()).thenReturn("memory://memory/test");
        when(workingCopy.getRev()).thenReturn("124");
        when(workingCopy.getBranch()).thenReturn("main");
        when(buildIdGenerator.buildId()).thenReturn("alibaba");

        f.feature(new Feature(C, T, K, N, D, 1, I));
        f.feature(new Feature(C, T, K, N, D, 1, I));

        verify(metadataApi).sendHeader(eq("memory://memory/test"), eq("124"), eq("main"), anyString()); // In CI we won't have alibaba, but the real build number.
    }

    @Test
    public void sends_test_step_and_test_case_result() throws Exception {
        Scenario s = new Scenario(C, T, K, N, D, 10, I);
        f.uri("the/path");
        f.startOfScenarioLifeCycle(s);
        f.scenario(s);
        f.step(new Step(C, K, N, 11, null, null));
        f.result(new Result("passed", 0L, null, null));
        f.step(new Step(C, K, N, 12, null, null));
        f.result(new Result("failed", 0L, new RuntimeException("This is the exception"), null));

        f.endOfScenarioLifeCycle(s);

        verify(metadataApi).sendTestStepResult("the/path", 12, Status.passed);
        verify(metadataApi).sendTestStepResult("the/path", 13, Status.failed);
        verify(metadataApi).sendStackTrace(eq("the/path"), eq(13), startsWith("java.lang.RuntimeException: This is the exception"));
        verify(metadataApi).sendTestCaseResult("the/path", 11, Status.failed);
    }

    @Test
    public void sends_binary_attachments() throws Exception {
        Scenario s = new Scenario(C, T, K, N, D, 10, I);
        f.uri("the/path");
        f.startOfScenarioLifeCycle(s);
        f.scenario(s);
        f.step(new Step(C, K, N, 11, null, null));
        f.embedding("image/png", new byte[]{1, 2, 3, 4, 5}); // Not a valid png but we don't care since we're not validating

        verify(metadataApi).sendMetadata("the/path", 12, "image/png");
        verify(metadataApi).sendBinary(new byte[]{1, 2, 3, 4, 5});
    }
}
