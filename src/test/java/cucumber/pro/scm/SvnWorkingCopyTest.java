package cucumber.pro.scm;

import org.junit.BeforeClass;
import org.junit.Test;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;

public class SvnWorkingCopyTest {

    private static File repoSourceDir = new File(ProjectRootDir.get(), "src/test/resources/svnrepo");
    private static File repoDir = new File(ProjectRootDir.get(), "target/svnrepo");
    private static File workingCopyDir = new File(ProjectRootDir.get(), "target/svnwc");

    @BeforeClass
    public static void importAndCheckout() throws IOException, InterruptedException {
        rmrf(repoDir);
        rmrf(workingCopyDir);

        ProcessBuilder svnAdminCreate = new ProcessBuilder("svnadmin", "create", repoDir.getAbsolutePath());
        run(svnAdminCreate);

        String importUrl = "file://" + repoDir.getAbsolutePath();
        ProcessBuilder svnImport = new ProcessBuilder("svn", "import", "--message", "importing", repoSourceDir.getAbsolutePath(), importUrl);
        run(svnImport);

        String checkoutUrl = "file://" + new File(repoDir, "trunk").getAbsolutePath();
        ProcessBuilder svnCheckout = new ProcessBuilder("svn", "checkout", checkoutUrl, workingCopyDir.getAbsolutePath());
        run(svnCheckout);
    }

    private static void run(ProcessBuilder processBuilder) throws IOException, InterruptedException {
        Process process = processBuilder.start();
        int exitStatus = process.waitFor();
        assertEquals(0, exitStatus);
    }

    @Test
    public void detects_wc_from_wc_sub_dir() {
        SvnWorkingCopy workingCopy = SvnWorkingCopy.detect(new File(workingCopyDir.getAbsolutePath(), "src/test/resources/foo"));
        assertEquals(workingCopyDir, workingCopy.getRootDir());
    }

    @Test
    public void detects_wc_from_wc_root_dir() {
        SvnWorkingCopy workingCopy = SvnWorkingCopy.detect(workingCopyDir);
        assertEquals(workingCopyDir, workingCopy.getRootDir());
    }

    @Test
    public void does_not_detect_wc_outside_wc_dir() {
        SvnWorkingCopy workingCopy = SvnWorkingCopy.detect(workingCopyDir.getParentFile());
        assertNull(workingCopy);
    }

    @Test
    public void finds_revision() throws IOException {
        WorkingCopy workingCopy = new SvnWorkingCopy(workingCopyDir);
        String head = workingCopy.getRev();
        assertEquals("1", head);
    }

    @Test
    public void finds_url() throws IOException {
        WorkingCopy workingCopy = new SvnWorkingCopy(workingCopyDir);
        String expected = "file://" + ProjectRootDir.get().getAbsolutePath() + "/target/svnrepo/trunk";
        assertEquals(expected, workingCopy.getRepoUrl());
    }

    @Test
    public void finds_branch() {
        WorkingCopy workingCopy = new SvnWorkingCopy(workingCopyDir);
        assertEquals("trunk", workingCopy.getBranch());
    }

    private static void rmrf(File f) throws IOException {
        if (!f.exists()) return;
        if (f.isDirectory()) {
            for (File c : f.listFiles())
                rmrf(c);
        }
        if (!f.delete())
            throw new FileNotFoundException("Failed to delete file: " + f);
    }
}
