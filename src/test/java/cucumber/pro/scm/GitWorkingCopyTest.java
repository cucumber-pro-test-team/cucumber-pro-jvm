package cucumber.pro.scm;

import org.junit.Before;
import org.junit.Test;

import java.io.File;
import java.io.IOException;
import java.util.Arrays;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.fail;

public class GitWorkingCopyTest {
    private WorkingCopy workingCopy;

    @Before
    public void create_scm() throws IOException {
        workingCopy = WorkingCopyDetector.detect();
    }

    @Test
    public void detects_wc_from_wc_sub_dir() {
        GitWorkingCopy workingCopy = GitWorkingCopy.detect(new File(ProjectRootDir.get(), "src/main/java"));
        assertEquals(ProjectRootDir.get(), workingCopy.getRootDir());
    }

    @Test
    public void detects_wc_from_wc_root_dir() {
        GitWorkingCopy workingCopy = GitWorkingCopy.detect(ProjectRootDir.get());
        assertEquals(ProjectRootDir.get(), workingCopy.getRootDir());
    }

    @Test
    public void does_not_detect_wc_outside_wc_dir() {
        GitWorkingCopy workingCopy = GitWorkingCopy.detect(ProjectRootDir.get().getParentFile());
        assertNull(workingCopy);
    }

    @Test
    public void finds_revision() throws IOException {
        String head = workingCopy.getRev();
        if (!head.matches("[0-9a-f]{40}")) {
            fail("Not a sha: [" + head + "]");
        }
    }

    @Test
    public void finds_url() throws IOException {
        String url = workingCopy.getRepoUrl();
        List<String> expected = Arrays.asList(
                "git@github.com:cucumber-ltd/cucumber-pro-jvm.git",
                "git://github.com/cucumber-ltd/cucumber-pro-jvm.git",
                "https://github.com/cucumber-ltd/cucumber-pro-jvm",
                "https://github.com/cucumber-ltd/cucumber-pro-jvm.git",
                "https://bitbucket.org/cucumber-pro-test-team/cucumber-pro-jvm.git"
        );
        for (String e : expected) {
            if (e.equals(url)) return;
        }
        fail("Bad url: " + url);
    }

    @Test
    public void finds_branch() {
        assertEquals("master", workingCopy.getBranch());
    }

}
