package cucumber.pro.scm;

import java.io.File;

public class ProjectRootDir {
    public static File get() {
        String classDir = ProjectRootDir.class.getProtectionDomain().getCodeSource().getLocation().getFile();
        return new File(classDir).getParentFile().getParentFile();
    }
}
