# Cucumber-JVM Plugin for Cucumber Pro

This formatter publishes results from [Cucumber-JVM](https://github.com/cucumber/cucumber-jvm) to [Cucumber Pro](https://cucumber.pro).

The easiest way to install the plugin is simply to press the "Activate Results" button in Cucumber Pro.
This guide explains how to install the plugin manually in the case where automatic installation fails for some reason.

## Installation

Always use the latest version. You can find it [here](http://search.maven.org/#search%7Cgav%7C1%7Cg%3A%22info.cukes%22%20AND%20a%3A%22cucumber-pro%22).

### Maven

Declare dependency in `pom.xml`:

```xml
<dependency>
    <groupId>info.cukes</groupId>
    <artifactId>cucumber-pro</artifactId>
    <version>VERSION</version>
    <scope>test</scope>
</dependency>
```

### Gradle

Declare dependency in `build.gradle`:

```groovy
dependencies {
    testCompile group: 'info.cukes', name: 'cucumber-pro', version: 'VERSION'
}
```

## Environment variables

There are a few environment variables that need to be set in order for the Cucumber Pro formatter to work. See below
for details about how to define environment variables.

## Enable the formatter with `CUCUMBER_OPTIONS`

Tell Cucumber to use the formatter:

    CUCUMBER_OPTIONS=--format cucumber.pro.CucumberProFormatter

The Cucumber Pro formatter is designed to only run in a Continous Integration (CI) environment. Because of this, it
will only publish results if the `CUCUMBER_PRO_TOKEN` environment variable is set. This means it won't publish anything
when developers and testers run cucumber locally on their machine (unless of course they define `CUCUMBER_PRO_TOKEN`).

## Authenticate with `CUCUMBER_PRO_TOKEN`

Cucumber Pro will only accept published results on behalf of a Cucumber Pro user account. This is done by telling the
formatter to use the API token of a Cucumber Pro user that has access to the source code repository.

Copy your personal API token from your [account page](https://app.cucumber.pro/my/profile) on Cucumber Pro
and assign it to the `CUCUMBER_PRO_TOKEN` environment variable:

    CUCUMBER_PRO_TOKEN=xxxxxx

If your repository is private you can add this to `cucumber.properties`.

If your repository is public, you should not add it to `cucumber.properties`, as this would allow anyone with read
access to the repository to publish results for it (even if they don't have write access to the repository).

If the repository is public and you are using Travis, you should set this in your `.travis.yml` using
[travis encrypt](http://about.travis-ci.org/docs/user/encryption-keys/).

## Continous Integration detection

The formatter will only publish results if it detects that it is running in a Continuous Integration environment.
If one of the following environment variables have a value, the formatter will assume it's running in CI:

* `BUILD_NUMBER` - Hudson/Jenkins/TeamCity
* `CIRCLE_BUILD_NUM` - Circle CI
* `TRAVIS_JOB_NUMBER` - Travis 
* `bamboo.buildNumber` - Bamboo
* `CI_BUILD_NUMBER` - Codeship
* `CI`

You should *not* define any of these environment variables in `cucumber.properties` - most CI servers will have one of
them defined already.

If your CI server doesn't do this, please refer to your CI server's documentation and define the `CI` environment
variable manually. You can also send us a pull request if you want the plugin to look for another environment variable.

## Override connection timeout

In some environments the DNS cache may not contain the host name of the Cucumber Pro results service. This may lead to a
connection timeout. The default timeout value is 10000 milliseconds. You can override the timeout value with an 
environment variable:

    CUCUMBER_PRO_CONNECTION_TIMEOUT_MILLIS=...

## Print debug information

You can tell the Cucumber Pro plugin to log what it's doing:

    CUCUMBER_PRO_DEBUG=true

This can be very useful to troubleshoot and verify that results are being sent successfully.

## Publish to Cucumber Pro appliance with `CUCUMBER_PRO_RESULTS_URL`

If you are hosting your own Cucumber Pro appliance you must tell `cucumber.pro.CucumberProFormatter` to publish
results to your appliance instead of the publicly hosted Cucumber Pro. You can do this with another environment
variable:

    CUCUMBER_PRO_RESULTS_URL=...

You will find the URL to use on the general Cucumber Pro setting page of your appliance.

## How to set Environment variables

Environment variables can be set in 4 different ways.

### In a cucumber.properties file

The preferred place to define Cucumber Pro environment variables is in a `cucumber.properties` file on your `CLASSPATH`.
For Maven projects it will automatically be on your `CLASSPATH` if you place it in `src/test/resources/cucumber.properties`
relative to your `pom.xml` file.

Example:

    CUCUMBER_OPTIONS=--format cucumber.pro.CucumberProFormatter

### As a JVM system property

For example:

    -Dcucumber.options="--format cucumber.pro.CucumberProFormatter"

Or alternatively:

    -DCUCUMBER_OPTIONS="--format cucumber.pro.CucumberProFormatter"

### On your workstation:

If you just want to try out the formatter without modifying any files, just set the variables in your shell:

Linux / OS X:

    export CUCUMBER_OPTIONS="--format cucumber.pro.CucumberProFormatter"
    export CUCUMBER_PRO_TOKEN="xxxxxx"
    export CI=true

Windows:

    SET CUCUMBER_OPTIONS="--format cucumber.pro.CucumberProFormatter"
    SET CUCUMBER_PRO_TOKEN="xxxxxx"
    SET CI=true

### On the Continuous Integration Server

Please refer to your CI server's documentation for details.
