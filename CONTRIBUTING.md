## Release process

Update `HISTORY.md` and commit.

    mvn release:clean
    mvn --batch-mode -P release-sign-artifacts release:prepare -DautoVersionSubmodules=true -DdevelopmentVersion=X.Y.Z-SNAPSHOT
    mvn -P release-sign-artifacts release:perform

Then go to https://oss.sonatype.org/#stagingRepositories, close and release.