## [1.0.14](https://github.com/cucumber-ltd/cucumber-pro-jvm/compare/v1.0.13...v1.0.14) (2014-08-26)

* Fix a bug in branch reporting
* Issue a debug message when all acks are received

## [1.0.13](https://github.com/cucumber-ltd/cucumber-pro-jvm/compare/v1.0.11...v1.0.13) (2014-08-14)

(The 1.0.12 release went wrong)

* Resove hostnames before trying to connect
* Override connection timeout with `CUCUMBER_PRO_CONNECTION_TIMEOUT_MILLIS`
* Print debug information with `CUCUMBER_PRO_DEBUG=true`

## [1.0.11](https://github.com/cucumber-ltd/cucumber-pro-jvm/compare/v1.0.10...v1.0.11) (2014-08-12)

* Rapid firing of messages without waiting for response until the end
* Better error detection (errors from server)
* Use CI server's build number if available
* Support new results API (use `build_id` instead of `group`)

## [1.0.10](https://github.com/cucumber-ltd/cucumber-pro-jvm/compare/v1.0.9...v1.0.10) (2014-07-07)

* Check `CI` as well as `CUCUMBER_PRO_TOKEN`.

## [1.0.9](https://github.com/cucumber-ltd/cucumber-pro-jvm/compare/v1.0.8...v1.0.9) (2014-06-28)

* Check `CUCUMBER_PRO_TOKEN`, no longer use `CI`.

## [1.0.8](https://github.com/cucumber-ltd/cucumber-pro-jvm/compare/v1.0.7...v1.0.8) (2014-06-28)

* Only check whether the working copy is clean if we're publishing (`CI` is defined).

## [1.0.7](https://github.com/cucumber-ltd/cucumber-pro-jvm/compare/v1.0.6...v1.0.7) (2014-05-23)

* Publish our own version
* Don't publish anyting unless `CI=true`
